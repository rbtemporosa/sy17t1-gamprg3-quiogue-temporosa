﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public GameObject koko;

	private Vector3 offset;

	void Start ()
    {
		offset = transform.position - koko.transform.position;
	}
	
	void LateUpdate ()
    {
		transform.position = koko.transform.position + offset;
	}
}
