﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimButtons : MonoBehaviour
{
    public Animator anim;

    public void Idle()
    {
        anim.Play("KK_Idle",0,0);
    }
    public void Attack()
    {
        anim.Play("KK_Attack",0,0);
    }
    public void Run()
    {
        anim.Play("KK_Run",0,0);
    }
    public void attackStdBy()
    {
        anim.Play("KK_Attack_Standy",0,0);
    }
    public void Combo()
    {
        anim.Play("KK_Combo",0,0);
    }
    public void Damage()
    {
        anim.Play("KK_Damage",0,0);
    }
    public void Dead()
    {
        anim.Play("KK_Dead",0,0);
    }
    public void drawBlade()
    {
        anim.Play("KK_DrawBlade",0,0);
    }
    public void Skill()
    {
        anim.Play("KK_Skill",0,0);
    }
    public void putBlade()
    {
        anim.Play("KK_PutBlade",0,0);
    }
    public void attackRun()
    {
        anim.Play("KK_Run_No",0,0);
    }
}
