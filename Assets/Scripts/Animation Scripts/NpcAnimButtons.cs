﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcAnimButtons : MonoBehaviour
{
	public Animator npcAnim1;
	public Animator npcAnim2;

	public void Idle()
    {
		npcAnim1.Play ("Idle", 0, 0);
		npcAnim2.Play ("Idle", 0, 0);
	}

	public void Talk()
    {
		npcAnim1.Play ("Talk", 0, 0);
		npcAnim2.Play ("Talk", 0, 0);
	}
}