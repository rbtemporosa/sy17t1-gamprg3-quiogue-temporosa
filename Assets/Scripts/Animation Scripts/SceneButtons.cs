﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneButtons : MonoBehaviour
{
	public void NPC()
    {
		SceneManager.LoadScene ("NPCAnim");
	}

	public void Monsters()
    {
		SceneManager.LoadScene ("MonstersAnim");
	}

	public void Player()
    {
		SceneManager.LoadScene ("KokoAnim");
	}
}