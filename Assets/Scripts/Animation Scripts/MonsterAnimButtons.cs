﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimButtons : MonoBehaviour
{
	public Animator goblinAnim;
	public Animator wildPigAnim;
	public Animator slimeAnim;

	public void Idle()
    {
		goblinAnim.Play ("GB_Idle", 0, 0);
		wildPigAnim.Play ("WP_Idle", 0, 0);
		slimeAnim.Play ("SL_Idle", 0, 0);
	}

	public void Walk()
    {
		goblinAnim.Play ("GB_Walk", 0, 0);
		wildPigAnim.Play ("WP_Walk", 0, 0);
		slimeAnim.Play ("SL_Walk", 0, 0);
	}

	public void attackRun()
    {
		goblinAnim.Play ("GB_Run", 0, 0);
		wildPigAnim.Play ("WP_Run", 0, 0);
		slimeAnim.Play ("SL_Run", 0, 0);
	}

	public void Attack()
    {
		goblinAnim.Play ("GB_Attack", 0, 0);
		wildPigAnim.Play ("WP_Attack", 0, 0);
		slimeAnim.Play ("SL_Attack", 0, 0);
	}

	public void Damage()
    {
		goblinAnim.Play ("GB_Damage", 0, 0);
		wildPigAnim.Play ("WP_Damage", 0, 0);
		slimeAnim.Play ("SL_Damage", 0, 0);
	}

	public void Skill()
    {
		goblinAnim.Play ("GB_Attack01", 0, 0);
		wildPigAnim.Play ("WP_Attack01", 0, 0);
		slimeAnim.Play ("SL_Attack01", 0, 0);
	}

	public void Dead()
    {
		goblinAnim.Play ("GB_Dead", 0, 0);
		wildPigAnim.Play ("WP_Dead", 0, 0);
		slimeAnim.Play ("SL_Dead", 0, 0);
	}
}
