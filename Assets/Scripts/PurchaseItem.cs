﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseItem : MonoBehaviour
{
    public InventorySystem Inventory;
    public GameObject potion;
    public GameObject koko;
    public int itemValue = 50;

    public void OnClick()
    {
        if (itemValue <= koko.GetComponent<GoldCurrency>().curgoldval)
        {
            koko.GetComponent<GoldCurrency>().curgoldval -= itemValue;
            Inventory.AddItem(GetComponent<Item>());
            Debug.Log("Purchased!");
        }
        else if (itemValue >= koko.GetComponent<GoldCurrency>().curgoldval)
        {
            Debug.Log("Not enough gold");
        }
    }
}