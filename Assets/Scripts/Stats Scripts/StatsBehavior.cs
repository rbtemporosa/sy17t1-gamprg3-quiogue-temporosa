﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsBehavior : BaseStats
{
    void Start()
    {
        maxHp += HealthPoints;
        curHp = maxHp;
    }
    void Update ()
    {
        if(curHp <= 0)
        {
            curHp = 0;
            Destroy(this.gameObject);
        }
	}

    public void ApplyDamage(int damage)
    {
        curHp -= damage;
    }

    public void ApplyHealth(int health)
    {
        curHp += health;
    }
}