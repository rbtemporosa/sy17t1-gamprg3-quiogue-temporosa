﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStats : MonoBehaviour
{
    public int Str;
    public int maxDamage;
    public int Vit;
    public int maxHp;
    public int curHp;

    public int AttackPower
    {
        get
        {
            return maxDamage += Str;
        }

        set
        {
            Str = value;
        }
    }

    public int HealthPoints
    {
        get
        {
            return Vit * 5;
        }

        set
        {
            Vit = value;
        }
    }
	
}
