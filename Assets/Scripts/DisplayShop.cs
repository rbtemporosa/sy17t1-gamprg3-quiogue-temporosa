﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DisplayShop : MonoBehaviour
{
    public GameObject shopController;
    bool showItemShop = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag ("Player"))
        {
            showItemShop = true;
            shopController.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            showItemShop = false;
            shopController.gameObject.SetActive(false);
        }
    }
}