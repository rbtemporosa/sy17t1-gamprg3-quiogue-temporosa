﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SkillCooldown : MonoBehaviour {
	public Image SkillCD;
	public float timer = 5;
	bool isCooldown;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			isCooldown = true;
		}
		if(isCooldown){
			SkillCD.fillAmount += 1 / timer * Time.deltaTime;

			if (SkillCD.fillAmount >= 1) {
				SkillCD.fillAmount = 0;
				isCooldown = false;
			}
		}
	}
}
