﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour {
	public float MPCost;
	public float Cooldown;

	public virtual void Use(GameObject actor){
		
		actor.GetComponent<Unit> ().MP -= MPCost;
	}
}
