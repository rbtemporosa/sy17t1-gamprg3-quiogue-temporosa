﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterMovement : MonoBehaviour
{
    public GameObject point;

    private Animator anim;
    private NavMeshAgent agent;
    private bool run;

    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    } 

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButtonDown(1))
        {
            if (Physics.Raycast(ray, out hit, 100))
            {
                run = true;
                point.transform.position = hit.point;
                agent.destination = hit.point;
            }
        }
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            run = false;
            point.gameObject.SetActive(false);
        }

        else
        {
            run = true;
            point.gameObject.SetActive(true);
        }
            anim.SetBool("Run", run);
    }
}