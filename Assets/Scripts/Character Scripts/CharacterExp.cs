﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CharacterExp : MonoBehaviour
{
    public float nextLevel = 100;
    public float currentXP = 0;
   // public float expgive = 0;

  //  public GameObject enemy;
    public GameObject[] btn;
    public Text levelTxt;
    public Text pointsallocate;

    public int levelUp = 1;
    public int pointAllo = 0;

    StatsBehavior health;
 
   // void Update()
   // {
     //   if (enemy.GetComponent<StatsBehavior>().curHp <= 0 && enemy != null)
    //    {
     //       currentXP += expgive;
       //     xpGain();
       // }
    //}

    public void AddXp(float value)
    {
		currentXP += value;
		xpGain ();
    }

    void xpGain()
    {
        if (currentXP >= nextLevel)
        {
            Levelup();
            levelUp += 1;
            levelTxt.text = "Level : " + levelUp.ToString();
            for (int i = 0; i < 2; i++)
            {
                if (pointAllo > 0)
                {
                    btn[i].gameObject.SetActive(true);
                }

                else if (pointAllo <= 0)
                {
                    btn[i].gameObject.SetActive(false);
                }
            }
        }
    }

    void Levelup()
    {
        nextLevel += 100;
        currentXP = 0;
        pointAllo += 1;
        pointsallocate.text = "Stat Points : " + pointAllo.ToString();
    }
}