﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KokoAttackBehavior : MonoBehaviour
{
	public Animator anim;

    int rayDistance = 2;
    int dmg;

	void Start ()
    {
        dmg = GetComponent<StatsBehavior>().AttackPower;
	}

    RaycastHit hit;

    void Update()
    {
        Debug.DrawRay(transform.position, transform.TransformVector(new Vector3(0, 0, rayDistance)), Color.green);
        Ray pointRay = new Ray(transform.position, transform.TransformDirection(new Vector3(0, 0, rayDistance)));

        if (Physics.Raycast(pointRay, out hit, rayDistance))
        {
            if (hit.collider.gameObject.tag == "Enemy")
            {
                DamageTo();
            }
        }
    }

    public void DamageTo()
    {
        anim.Play("KK_Attack");
        Debug.Log("Hit!");
        hit.transform.gameObject.SendMessage("ApplyDamage", dmg, SendMessageOptions.DontRequireReceiver);
    }
}