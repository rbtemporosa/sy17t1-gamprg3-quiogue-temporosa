﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCrush : Skill {
	
	public GameObject EffectPrefab;
	public int Damage = 0;
	public float Radius = 15.0f;
	public LayerMask mask;

	void Update(){
		
		//SkillCollide (transform.position, Radius, mask);
	}

	public override void Use(GameObject actor){
		base.Use (actor);

		//Damage += GetComponent<StatsBehavior> ().maxDamage;
		SkillCollide (transform.position, Radius, mask);

	}
	void SkillCollide(Vector3 center, float radius, LayerMask mask){

		Damage = GetComponent<StatsBehavior> ().maxDamage;

		Collider[] colliders = Physics.OverlapSphere (center, radius, mask);
		int i = 0;
		while(i < colliders.Length){
			Debug.Log (colliders[i].gameObject.name);
			colliders [i].SendMessage ("HealthDecrease", Damage);
			i++;

			Debug.Log (colliders.Length);
		}
		foreach(Collider other in colliders){
			other.gameObject.GetComponent<StatsBehavior> ().curHp -= Damage;

		}
	}
}
