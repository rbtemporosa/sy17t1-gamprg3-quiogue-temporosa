﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterClickBehavior : MonoBehaviour {

    public GameObject selectedUnit;

    private NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            SelectTarget();
        }
    }

    void SelectTarget()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 10000))
        {
            if (hit.transform.tag == "Enemy")
            {
                //this.transform.position = selectedUnit.transform.position;
                selectedUnit = hit.collider.transform.gameObject;
                //agent.destination = hit.point;
            }
        }
    }
}