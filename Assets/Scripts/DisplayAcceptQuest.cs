﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayAcceptQuest : MonoBehaviour
{
    public GameObject acceptQuest;
    bool showAcceptQuest = false;

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag ("Player"))
        {
            showAcceptQuest = true;
            acceptQuest.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            showAcceptQuest = false;
            acceptQuest.gameObject.SetActive(false);
        }
    }

    public void ClosePanel()
    {
        showAcceptQuest = false;
        acceptQuest.gameObject.SetActive(false);
    }
}
