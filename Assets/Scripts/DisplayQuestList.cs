﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayQuestList : MonoBehaviour
{
    public GameObject questList;
    bool showQuestList = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ShowQuestPanel();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseQuestPanel();
        }
    }

    public void ShowQuestPanel()
    {
        showQuestList = true;
        questList.gameObject.SetActive(true);
    }

    void CloseQuestPanel()
    {
        showQuestList = false;
        questList.gameObject.SetActive(false);
    }
}
