﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour, IPointerClickHandler
{
    private Stack<Item> items;

    public Text stackIndicator;
    public Sprite emptySlots;
    public Sprite highlightedSlot;
    public GameObject Koko;

    public bool IsEmpty
    {
        get
        {
            return Items.Count == 0;
        }
    }

    public bool IsAvailable
    {
        get
        {
            return CurrentItem.maxSize > Items.Count;
        }
    }

    public Item CurrentItem
    {
        get
        {
            return Items.Peek();
        }
    }

    public Stack<Item> Items
    {
        get
        {
            return items;
        }

        set
        {
            items = value;
        }
    }

    private void Start()
    {
        Items = new Stack<Item>();
        RectTransform slotRect = GetComponent<RectTransform>();
        RectTransform textRect = stackIndicator.GetComponent<RectTransform>();

        int textScale = (int)(slotRect.sizeDelta.x * 0.60);
        stackIndicator.resizeTextMaxSize = textScale;
        stackIndicator.resizeTextMinSize = textScale;

        textRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotRect.sizeDelta.x);
        textRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotRect.sizeDelta.y);
    }

    public void AddItem(Item item)
    {
        Items.Push(item);

        if (Items.Count > 1)
        {
            stackIndicator.text = Items.Count.ToString(); //Shows how many items on the stack
        }

        ChangeSprite(item.currentSprite, item.highlightedSprite);
    }

    private void ChangeSprite(Sprite current, Sprite highlight)
    {
        GetComponent<Image>().sprite = current;

        SpriteState st = new SpriteState();
        st.highlightedSprite = highlight;
        st.pressedSprite = current;

        GetComponent<Button>().spriteState = st;
    }

    private void ConsumeItem()
    {
        if (!IsEmpty)
        {
            Items.Pop().Use();

            stackIndicator.text = Items.Count > 1 ? Items.Count.ToString() : string.Empty;

            if (IsEmpty)
            {
                ChangeSprite(emptySlots, highlightedSlot);
                InventorySystem.EmptySlots++;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            ConsumeItem();
        }
    }
}
