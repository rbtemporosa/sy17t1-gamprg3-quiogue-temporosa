﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemType {health, gold};

public class Item : MonoBehaviour
{
    public ItemType type;
    public Sprite currentSprite;
    public Sprite highlightedSprite;
    public int maxSize;
    public string itemName;
    public string Description;

    public void Use()
    {
        switch (type)
        {
            case ItemType.health:
                Debug.Log("Consumed Health Potion");
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

    public string GetTooltip()
    {
        string stats = string.Empty;
        string color = string.Empty;
        string newline = string.Empty;

        if (Description != string.Empty)
        {
            newline = "\n";
        }
        return string.Format("<size=16>(0)</size><size=14><i><color=lime>" + newline + "(1)</color></i>",itemName,Description);
    }
}