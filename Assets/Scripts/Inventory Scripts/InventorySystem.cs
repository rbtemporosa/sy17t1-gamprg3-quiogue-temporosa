﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{
    private RectTransform inventoryRect;
    private List<GameObject> items;
    private float inventoryWidth, inventoryHeight;

    public int slots;
    public int rows;
    public float inventoryPaddingLeft;
    public float inventoryPaddingTop;
    public float inventorySize;
    public GameObject inventoryPrefab;
    public GameObject toolobject;
    public Text sizetextobj;
    public Text visualtextobj;

    private static int emptySlots;
    private static GameObject tooltip;
    private static Text sizetext;
    private static Text visualtext;

    public static int EmptySlots
    {
        get
        {
            return emptySlots;
        }

        set
        {
            emptySlots = value;
        }
    }
    // Use this for initialization
    void Start ()
    {
        tooltip = toolobject;
        sizetext = sizetextobj;
        visualtext = visualtextobj;
        CreateLayout();
	}

    public void showTooltip(GameObject slot)
    {
        ItemSlot tempslot = slot.GetComponent<ItemSlot>();

        if (!tempslot.IsEmpty)
        {
            visualtext.text = tempslot.CurrentItem.GetTooltip();
            sizetext.text = visualtext.text;
            tooltip.SetActive(true);

            float xPos = slot.transform.position.x + inventoryPaddingLeft;
            float yPos = slot.transform.position.y - slot.GetComponent<RectTransform>().sizeDelta.y - inventoryPaddingTop;
            tooltip.transform.position = new Vector2(xPos, yPos);
        }
    }

    public void hideTooltip()
    {
        tooltip.SetActive(false);
    }

    private void CreateLayout()
    {
        items = new List<GameObject>();

        EmptySlots = slots;

        inventoryWidth = (slots / rows) * (inventorySize + inventoryPaddingLeft) + inventoryPaddingLeft;
        inventoryHeight = rows * (inventorySize + inventoryPaddingTop) + inventoryPaddingTop;

        inventoryRect = GetComponent<RectTransform>();
        inventoryRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, inventoryWidth);
        inventoryRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, inventoryHeight);

        int columns = slots / rows;

        for(int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                GameObject newSlot = (GameObject)Instantiate(inventoryPrefab); //Instantiates the slotPrefab and saves it to newSlot
                RectTransform slotRect = newSlot.GetComponent<RectTransform>(); //Getting the RectTransform of the slotPrefab

                newSlot.name = "ItemSlot"; //Changes the name
                newSlot.transform.SetParent(this.transform.parent);

                slotRect.localPosition = inventoryRect.localPosition + new Vector3(inventoryPaddingLeft * (x + 1) + (inventorySize * x),
                    -inventoryPaddingTop * (y + 1) - (inventorySize * y));

                slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, inventorySize);
                slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, inventorySize);

                items.Add(newSlot);
            }
        }
    }

    public bool AddItem(Item item)
    {
        if (item.maxSize == 1)
        {
            AvailableSlot(item);
            return true;
        }
        else
        {
            foreach (GameObject slot in items)
            {
                ItemSlot temporary = slot.GetComponent<ItemSlot>();

                if (!temporary.IsEmpty)
                {
                    if (temporary.CurrentItem.type == item.type && temporary.IsAvailable)
                    {
                        temporary.AddItem(item);
                        return true;
                    }
                }
            }

            if (EmptySlots > 0)
            {
                AvailableSlot(item);
            }
        }

        return false;
    }

    private bool AvailableSlot(Item item)
    {
        if (EmptySlots > 0)
        {
            foreach (GameObject slot in items)
            {
                ItemSlot temporary = slot.GetComponent<ItemSlot>();

                if (temporary.IsEmpty)
                {
                    temporary.AddItem(item);
                    //EmptySlots--;
                    return true;
                }
            }
        }

        return false;
    }
}