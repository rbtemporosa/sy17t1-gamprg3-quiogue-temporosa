﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Droptable : MonoBehaviour {
  //  public GameObject interactable;
	public GameObject stat;
	public GameObject gold;
    [System.Serializable]
    public class Droptble {
        public string name;
        public GameObject item;
        public int rarity;
    }

	void Update(){
		if(this.gameObject.GetComponent<StatsBehavior>().curHp <= 0){
			Debug.Log ("dropped shit");
			droprate ();
			Instantiate (gold.gameObject, this.transform.position, Quaternion.identity);
		}
	}

    public List<Droptble> loottable = new List<Droptble>();
    public int dropchance;

    void droprate() {
        int chance = Random.Range(0, 101);
        if (chance > dropchance) {
            Debug.Log("No luck");
            return;
        }
		if (chance <= dropchance) {
            int items = 0;
			int currency = 0;
            for (int i = 0; i < loottable.Count; i++) {
                items += loottable[i].rarity;
            }
            Debug.Log("Item = " + items);
			int randvalue = Random.Range(0, items);

            for (int k = 0; k < loottable.Count; k++) {
                if (randvalue <= loottable[k].rarity) {
					Instantiate(loottable[k].item, this.transform.position, Quaternion.identity);
                }
                randvalue -= loottable[k].rarity;
				Debug.Log("Random Value decreased(Items)" + randvalue);
            }
        }
    }
}
