﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum Itemtype {HealthPot };

public class ItemDescription : MonoBehaviour
{
    
    private string text = "Restores 50 HP";
    private string currentToolTipText = "";

    public GameObject tooltip;
    public Text sizetext;
    public Text visualtext;

    public string itemName;
    public string Description;

    public string GetTooltip() {
        string stats = string.Empty;
        string color = string.Empty;
        string newline = string.Empty;

        if (Description != string.Empty) {
            newline = "\n";
        }
      return string.Format("<color=" + color + "><size=16>(0)</size></color><size=14><i><color=lime>" + newline + "(1)</color></i>", itemName, Description);
    }  
}
