﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyUnit : MonoBehaviour {
	public float xpVal = 50f;

	// Update is called once per frame
	void Update () {
		if(this.gameObject.GetComponent<StatsBehavior>().curHp <= 0){
			Debug.Log ("Gave xp");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<CharacterExp> ().AddXp (xpVal);
			Destroy (this.gameObject);
		}		
	}
}
