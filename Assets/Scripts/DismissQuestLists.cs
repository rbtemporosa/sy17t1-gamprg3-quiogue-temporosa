﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DismissQuestLists : MonoBehaviour
{
    public GameObject killSlimes;
    public GameObject killGoblins;
    public GameObject killWildPigs;
    public GameObject killMonsters;

    public void DismissSlimes()
    {
        killSlimes.gameObject.SetActive(false);
    }

    public void DismissGoblins()
    {
        killGoblins.gameObject.SetActive(false);
    }

    public void DismissWildPigs()
    {
        killWildPigs.gameObject.SetActive(false);
    }

    public void DismissMonsters()
    {
        killMonsters.gameObject.SetActive(false);
    }
}
