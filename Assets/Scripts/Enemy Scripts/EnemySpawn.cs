﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawn : MonoBehaviour
{
    public GameObject[] spawns;
    public GameObject spawner;
    public Vector3 spawnVal;
    public bool stop;

    void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(6);
        while (!stop)
        {
            Vector3 spawnPos = new Vector3(Random.Range(-spawnVal.x, spawnVal.x), 1, Random.Range(-spawnVal.z, spawnVal.z));
            Instantiate(spawns[Random.Range(0, spawns.Length)], spawnPos + transform.TransformPoint(0, 0, 0), 
                gameObject.transform.rotation);
            yield return new WaitForSeconds(6);
        }
    }
}
