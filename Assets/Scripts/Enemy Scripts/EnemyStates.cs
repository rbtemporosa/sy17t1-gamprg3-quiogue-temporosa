﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyStates : MonoBehaviour
{
    enum States
    {
        Attack,
        Chase,
        Patrol
    }

    public Transform player;

    private States currentStates;
    private Animator anim;
    private float timer = 1f;
    private float max_timer = 13f;
    private NavMeshAgent agent;
    private Vector3 point;
    private Vector3 startPosition;
    private bool wander;
    private bool chase = false;
    private float wanderspd = 0.5f;
    private GameObject target;
	private NavMeshAgent nav;
	private int roamRadius = 1;

//	Vector3 startingposition;

    int rayDistance = 2;
    int enemyDamage;

    RaycastHit hit;

    void Start ()
    {
        anim = GetComponent<Animator>();
		currentStates = States.Chase;
        agent = GetComponent<NavMeshAgent>();
        enemyDamage = GetComponent<StatsBehavior>().AttackPower;
	}

    void Update()
    {
        if (currentStates == States.Attack)
            Attack();
        else if (currentStates == States.Chase)
            Chase();
        else if (currentStates == States.Patrol)
            Patrol();

        Debug.DrawRay(transform.position, transform.TransformVector(new Vector3(0, 0, rayDistance)), Color.green);
        Ray pointRay = new Ray(transform.position, transform.TransformDirection(new Vector3(0, 0, rayDistance)));

        if (Physics.Raycast(pointRay, out hit, rayDistance))
        {
            if (hit.collider.gameObject.tag == "Player")
            {
                Attack();
            }
        }
    }

    void Attack()
    {
        Vector3 direction = player.position - this.transform.position;
        if (direction.magnitude <= 1.5)
        {
			anim.SetTrigger ("Attack");
		}

        else if(direction.magnitude > 1.5)
        {
			currentStates = States.Chase;
            hit.transform.gameObject.SendMessage("ApplyDamage", enemyDamage, SendMessageOptions.RequireReceiver);
        }
    }

    void Chase()
    {
        if (Vector3.Distance(player.position, this.transform.position) < 15)
        {
            Vector3 direction = player.position - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
            if (direction.magnitude > 1.5)
            {
                this.transform.Translate(0, 0, 0.04f);
                anim.SetTrigger("Chase");
                //currentStates = States.Chase;
            }

            else if (direction.magnitude <= 1.5)
            {
                currentStates = States.Attack;
            }
        }

        else if (Vector3.Distance(player.position, this.transform.position) > 15)
        {
			anim.SetTrigger ("Idle");
        }
    }

    void Patrol()
    {

		if (Vector3.Distance (player.position, this.transform.position) > 15) {
		//	Roam ();
	}
		else if(Vector3.Distance (player.position, this.transform.position) < 15){
			currentStates = States.Chase;
		}
    }
	void Roam(){
		{
			Debug.Log ("Roaming");
			Vector3 randomDirection = Random.insideUnitSphere * roamRadius;
			randomDirection = startPosition;
			NavMeshHit hit;
			NavMesh.SamplePosition (randomDirection, out hit, roamRadius, 1);
			Vector3 finalPosition = hit.position;
			nav.destination = finalPosition;
		}
	}

}
