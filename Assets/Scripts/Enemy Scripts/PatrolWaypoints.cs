﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolWaypoints : MonoBehaviour
{
	public Transform [] Waypoints;
	public float speed;
	public int curWayPoint;
	public bool doPatrol = true;
	public Vector3 Target;
	public Vector3 MoveDirection;
	public Vector3 Velocity;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start ()
    {
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (curWayPoint < Waypoints.Length)
        {
			Target = Waypoints [curWayPoint].position;
			MoveDirection = Target - transform.position;
			Velocity = rigidbody.velocity;

			if (MoveDirection.magnitude < 1)
            {
				curWayPoint++;
			} 

			else
            {
				Velocity = MoveDirection.normalized * speed;
			}
		}

		else
        {
			if (doPatrol)
            {
				curWayPoint = 0;
			}

			else
            {
				Velocity = Vector3.zero;
			}

		}

		rigidbody.velocity = Velocity;
		transform.LookAt (Target);
	}
}
